#!/usr/bin/env python3
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# __author__ = 'holger.fischer@hoonet.org'

## IMPORTS
import argparse

import pyghmi.ipmi.bmc as ipmibmc
import pyghmi.ipmi.command as ipmicommand
import sys
import logging
import os
import time

import pprint

from xml.etree import ElementTree

try:
        import pywsman
except ImportError:
        pass

## LOGGING
loglevel=logging.WARNING

logging.basicConfig(level=loglevel,
        format=
        '%(asctime)s.%(msecs)03d %(levelname)s '
        '%(module)s - %(funcName)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

LOG = logging.getLogger()

## CONSTANTS
# FIXME where should this be used?
#IPMI_COMMAND_NODE_BUSY = 0xC0

BMC_AWAKE_INTERVAL = 60
BMC_AWAKE_CACHE = {}

# POWER states
# 1:  Other
# 2:  On
# 3:  Sleep - Light
# 4:  Sleep - Deep
# 5:  Power Cycle (Off - Soft)
# 6:  Off - Hard
# 7:  Hibernate (Off - Soft)
# 8:  Off - Soft
# 9:  Power Cycle (Off-Hard)
# 10: Master Bus Reset
# 11: Diagnostic Interrupt (NMI)
# 12: Off - Soft Graceful
# 13: Off - Hard Graceful
# 14: Master Bus Reset Graceful
# 15: Power Cycle (Off-Soft Graceful)
# 16: Power Cycle (Off-Hard Graceful)
# 17: Diagnostic Interrupt (INIT)

# POWER ON states: system is on or will be 
POWER_ON_STATES=[2,3,4,5,7,9,10,14,15,16]

# POWER OFF states: system is off or will be 
POWER_OFF_STATES=[6,8,12,13]

POWER_ON='2'
POWER_OFF='8'
POWER_CYCLE='10'
POWER_DIAG='11'
POWER_OFF_GRACEFUL='12'
POWER_CYCLE_GRACEFUL='14'
POWER_ERROR='error'

RET_SUCCESS = '0'

_SOAP_ENVELOPE = 'http://www.w3.org/2003/05/soap-envelope'

XML_NS_ENUMERATION = "http://schemas.xmlsoap.org/ws/2004/09/enumeration"

CIM_PowerManagementService           = ('http://schemas.dmtf.org/wbem/wscim/1/'
                                        'cim-schema/2/'
                                        'CIM_PowerManagementService')
CIM_ComputerSystem                   = ('http://schemas.dmtf.org/wbem/wscim/'
                                        '1/cim-schema/2/CIM_ComputerSystem')
CIM_AssociatedPowerManagementService = ('http://schemas.dmtf.org/wbem/wscim/'
                                        '1/cim-schema/2/'
                                        'CIM_AssociatedPowerManagementService')

CIM_BootConfigSetting                = ('http://schemas.dmtf.org/wbem/wscim/'
                                        '1/cim-schema/2/CIM_BootConfigSetting')
CIM_BootSourceSetting                = ('http://schemas.dmtf.org/wbem/wscim/'
                                        '1/cim-schema/2/CIM_BootSourceSetting')

CIM_BootService                      = ('http://schemas.dmtf.org/wbem/wscim/'
                                        '1/cim-schema/2/CIM_BootService')

_ADDRESS = 'http://schemas.xmlsoap.org/ws/2004/08/addressing'
_ANONYMOUS = 'http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous'
_WSMAN = 'http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd'

## FUNCTIONS
def awake_bmc(bmc_address):
    """Wake up BMC with ping
    """

    now = int(time.time())
    last_awake = BMC_AWAKE_CACHE.get(bmc_address, 0)
    diff_awake = now - last_awake

    LOG.debug('\n    bmc_address(type: {}): {}\n'
            '    now(type: {}) {}\n'
            '    last_awake(type: {}) {}\n'
            '    diff_awake(type: {}) {}\n'
            '    BMC_AWAKE_INTERVAL(type: {}) {}\n'
            '    BMC_AWAKE_CACHE(type: {})\n'
            '        {}\n\n'.format(
                    type(bmc_address), bmc_address,
                    type(now), now, 
                    type(last_awake), last_awake, 
                    type(diff_awake), diff_awake, 
                    type(BMC_AWAKE_INTERVAL), BMC_AWAKE_INTERVAL, 
                    type(BMC_AWAKE_CACHE), pprint.pformat(BMC_AWAKE_CACHE)))

    if diff_awake > BMC_AWAKE_INTERVAL:
        try:
            os.system('ping -i 0.2 -c 5 {}'.format(bmc_address))
        except Exception as e:
            LOG.exception('Unable to awake BMC interface on bmc_address '
                      '{}. Error: {}'.format(
                      bmc_address, e))
            raise Exception
        else:
            LOG.debug('Successfully awakened BMC interface on bmc_address '
                       '{}.'.format(bmc_address))
            BMC_AWAKE_CACHE[bmc_address] = now

def xml_find(doc, namespace, item):
    """Find the first element with namespace and item, in the XML doc

    :param doc: a doc object.
    :param namespace: the namespace of the element.
    :param item: the element name.
    :returns: the element object or None
    """
    if doc is None:
        raise Exception
    tree = ElementTree.fromstring(doc.root().string())
    query = ('.//{%(namespace)s}%(item)s' % {'namespace': namespace,
                                             'item': item})
    return tree.find(query)

## CLASSES
class AmtClient(object):
    """AMT client.

    Create a pywsman client to connect to the target server
    """
    def __init__(self, 
            out_address, 
            out_protocol, 
            out_username, 
            out_password):

        AMT_PROTOCOL_PORT_MAP = {
            'http': 16992,
            'https': 16993,
        }

        out_port = AMT_PROTOCOL_PORT_MAP[out_protocol]

        path = '/wsman'
        if isinstance(out_protocol, str):
            out_protocol = str(out_protocol.encode(), 'utf-8')
        
        LOG.debug('\n    out_address(type: {}): {}\n'
                '    out_port(type: {}) {}\n'
                '    path(type: {}) {}\n'
                '    out_protocol(type: {}) {}\n'
                '    out_username(type: {}) {}\n'
                '    out_password(type: {}) {}\n\n'.format(
                    type(out_address), out_address,
                    type(out_port), out_port, 
                    type(path), path, 
                    type(out_protocol), out_protocol, 
                    type(out_username), out_username, 
                    type(out_password), '<out_password hidden>'))

        self.client = pywsman.Client(
                out_address, 
                out_port, 
                path, 
                out_protocol,
                out_username, 
                out_password)

        # do not love swig too much, 
        # because cannot find correct parameter names
        #self.client = pywsman.Client(
        #        hostname = out_address, 
        #        port     = out_port, 
        #        path     = path, 
        #        scheme   = out_protocol,
        #        username = out_username, 
        #        password = out_password)


    def wsman_enumerate(self, resource_uri, options=None):
        """Enumerate target server info

        :param options: client options
        :param resource_uri: a URI to an XML schema
        :returns: XmlDoc object
        """
        if options is None:
            options = pywsman.ClientOptions()

        LOG.debug('\n    options = pywsman.ClientOptions()"\n'
                  '    options(type: {})\n'
                  '        {}\n\n'.format(
                  type(options), pprint.pformat(options))) 
        
        doc = self.client.enumerate(options, None, resource_uri)
        root = doc.root()
        assert root is not None
        context = root.find(XML_NS_ENUMERATION, "EnumerationContext" )
        doc = self.client.pull( options , None, resource_uri, context.__str__())
        assert doc is not None
        root = doc.root()
        assert root is not None
        pullresp = root.find(XML_NS_ENUMERATION, "PullResponse" )
        assert pullresp is not None
        LOG.debug( '\nPull response:'
                '\n{}'.format(
                    pullresp.string() ))

        LOG.debug('\n    parameters and result for "doc = self.client.enumerate'
                  '(options, resource_uri)"\n'
                  '    resource_uri(type: {})\n'
                  '        {}\n'
                  '    doc(type: {})\n'
                  '        {}\n\n'.format(
                  type(resource_uri), resource_uri, 
                  type(doc), doc)) 

        item = 'Fault'
        fault = xml_find(doc, _SOAP_ENVELOPE, item)
        if fault is not None:
            LOG.error('Call to out BMC with URI {} failed: '
                      'got Fault {}'.format(
                      resource_uri, fault.text))
            raise Exception
        return doc

    def wsman_get(self, resource_uri, options=None):
        """Get target server info

        :param options: client options
        :param resource_uri: a URI to an XML schema
        :returns: XmlDoc object
        """
        if options is None:
            options = pywsman.ClientOptions()

        LOG.debug('\n    options = pywsman.ClientOptions()"\n'
                  '    options(type: {})\n'
                  '        {}\n\n'.format(
                  type(options), pprint.pformat(options))) 
        
        doc = self.client.get(options, resource_uri)

        LOG.debug('\n    parameters and result for "doc = self.client.get'
                  '(options, resource_uri)"\n'
                  '    resource_uri(type: {})\n'
                  '        {}\n'
                  '    doc(type: {})\n'
                  '        {}\n\n'.format(
                  type(resource_uri), resource_uri, 
                  type(doc), doc)) 

        item = 'Fault'
        fault = xml_find(doc, _SOAP_ENVELOPE, item)
        if fault is not None:
            LOG.error('Call to out BMC with URI {} failed: '
                      'got Fault {}'.format(
                      resource_uri, fault.text))
            raise Exception
        return doc

    def wsman_invoke(self, options, resource_uri, method, data=None):
        """Invoke method on target server

        :param options: client options
        :param resource_uri: a URI to an XML schema
        :param method: invoke method
        :param data: a XmlDoc as invoke input
        :returns: XmlDoc object
        """
        if data is None:
            doc = self.client.invoke(options, resource_uri, method)
        else:
            doc = self.client.invoke(options, resource_uri, method, data)
        item = "ReturnValue"
        return_value = xml_find(doc, resource_uri, item).text
        if return_value != RET_SUCCESS:
            LOG.error("Call to out BMC with URI {} and "
                      "method {} failed: return value "
                      "was {}".format(
                      resource_uri, method,
                      return_value))
            raise Exception
        return doc

class IpmiToAmt(ipmibmc.Bmc):
    #default,none    
    #network,pxe    
    #hd,disk     
    #safe,safe        
    #optical,cdrom            
    #setup,bios
    BOOT_DEVICE_MAP_NUM = {
        'default': 0x0,
        'network': 4,
        'hd': 8,
        'safe': 0xc,
        'optical': 0x14,
        'setup': 0x18,
    }
    
    BOOT_DEVICE_MAP_IPMI_AMT = {
        'network': 'Intel(r) AMT: Force PXE Boot',
        'hd': 'Intel(r) AMT: Force Hard-drive Boot',
        'optical': 'Intel(r) AMT: Force CD/DVD Boot',
    }

    AMT_MAX_ATTEMPTS = 5
    AMT_ACTION_WAIT = 1

    AMT_ACTION_INTERVAL = 60
    AMT_ACTION_CACHE = {}

    """
    Class for converting IPMI requests to AMT requests
    
    Supported IPMI commands:

    power status
    power on
    power status
    mc reset cold
    """

    def __init__(self, 
            in_address,
            in_port, 
            authdata, 
            out_address, 
            out_protocol, 
            out_username, 
            out_password):
        super(IpmiToAmt, self).__init__(
                address=in_address,
                port=in_port, 
                authdata=authdata) 

        self.client = AmtClient(
                out_address, 
                out_protocol,
                out_username, 
                out_password)

        self.out_address = out_address

        self.ipmi_boot_device = 'default'

    def get_boot_device(self):

        # we only get the possible boot devices for the next boot with this query
        #awake_bmc(self.out_address)
        #namespace = CIM_BootSourceSetting 
        #try:
        #    doc = self.client.wsman_enumerate(namespace)
        #except Exception as e:
        #    LOG.exception("Failed to get power state for out_address {} "
        #                  "with error: {}.".format(
        #                  self.out_address, e))

        LOG.debug('boot device name: {}'.format(
            self.ipmi_boot_device))
        
        try:
            ipmi_boot_device_num = self.BOOT_DEVICE_MAP_NUM[self.ipmi_boot_device]
            LOG.info('ipmi_boot_device_num: {}'.format(
                ipmi_boot_device_num))
            return ipmi_boot_device_num
        except Exception as e:
            LOG.exception('Failed: ipmi_boot_device: "{}" not in BOOT_DEVICE_MAP_NUM, error "{}"'.format(
                self.ipmi_boot_device,
                e))
            raise Exception
            return 0

    def _generate_change_boot_order_input(self, amt_boot_device):
        """Generate Xmldoc as change_boot_order input.
    
        This generates a Xmldoc used as input for _set_boot_device_order.
    
        :param amt_boot_device: the boot device.
        :returns: Xmldoc.
        """
        method_input = "ChangeBootOrder_INPUT"
        namespace = CIM_BootConfigSetting
        doc = pywsman.XmlDoc(method_input)
        root = doc.root()
        root.set_ns(namespace)
    
        child = root.add(namespace, 'Source', None)
        child.add(_ADDRESS, 'Address', _ANONYMOUS)
    
        grand_child = child.add(_ADDRESS, 'ReferenceParameters', None)
        grand_child.add(_WSMAN, 'ResourceURI', CIM_BootSourceSetting)
        g_grand_child = grand_child.add(_WSMAN, 'SelectorSet', None)
        g_g_grand_child = g_grand_child.add(_WSMAN, 'Selector', amt_boot_device)
        g_g_grand_child.attr_add(_WSMAN, 'Name', 'InstanceID')
        return doc
     
    def _set_boot_device_order(self, ipmi_boot_device):
        """Set boot device order configuration of AMT Client.
    
        :param ipmi_boot_device: the boot device
        :raises: Exception
        """

        awake_bmc(self.out_address)

        amt_boot_device = self.BOOT_DEVICE_MAP_IPMI_AMT[ipmi_boot_device]

        LOG.debug('\namt_boot_device(type {}):\n'
                '    {}'.format(
                    type(amt_boot_device), amt_boot_device))
    
        doc = self._generate_change_boot_order_input(amt_boot_device)

        LOG.debug('\ndoc(type {}):\n'
                '    {}'.format(
                    type(doc), doc))
    
        method = 'ChangeBootOrder'
    
        options = pywsman.ClientOptions()
        options.add_selector('InstanceID', 'Intel(r) AMT: Boot Configuration 0')
    
        try:
            client_doc = self.client.wsman_invoke(
                    options, 
                    CIM_BootConfigSetting,
                    method, 
                    doc)
        except Exception as e:
            LOG.exception("Failed to set ipmi_boot_device {} ({}) for out_address {} "
                          "with error: {}.".format(
                          ipmi_boot_device, amt_boot_device, self.out_address, e))
            raise Exception

        else:
            LOG.info("Completed: set ipmi_boot_device {} ({}) for out_address {}".format(
                     ipmi_boot_device, amt_boot_device, self.out_address))
    
    def _generate_enable_boot_config_input(self):
        """Generate Xmldoc as enable_boot_config input.
    
        This generates a Xmldoc used as input for enable_boot_config.
    
        :returns: Xmldoc.
        """
        method_input = "SetBootConfigRole_INPUT"
        namespace = CIM_BootService
        doc = pywsman.XmlDoc(method_input)
        root = doc.root()
        root.set_ns(namespace)
    
        child = root.add(namespace, 'BootConfigSetting', None)
        child.add(_ADDRESS, 'Address', _ANONYMOUS)
    
        grand_child = child.add(_ADDRESS, 'ReferenceParameters', None)
        grand_child.add(_WSMAN, 'ResourceURI', CIM_BootConfigSetting)
        g_grand_child = grand_child.add(_WSMAN, 'SelectorSet', None)
        g_g_grand_child = g_grand_child.add(_WSMAN, 'Selector',
                                            'Intel(r) AMT: Boot Configuration 0')
        g_g_grand_child.attr_add(_WSMAN, 'Name', 'InstanceID')
        root.add(namespace, 'Role', '1')
        return doc
    
    def _enable_boot_config(self):
        """Enable boot configuration of AMT Client.
    
        :raises: Exception
        """

        awake_bmc(self.out_address)

        doc = self._generate_enable_boot_config_input()

        LOG.debug('\ndoc(type {}):\n'
                '    {}'.format(
                    type(doc), doc))
    
        method = 'SetBootConfigRole'

        options = pywsman.ClientOptions()
        options.add_selector('Name', 'Intel(r) AMT Boot Service')

        try:
            client_doc = self.client.wsman_invoke(
                    options, 
                    CIM_BootService,
                    method, 
                    doc)

        except Exception as e:
            LOG.exception("Failed to enable boot config for out_address {} "
                          "with error: {}.".format(
                          self.out_address, e))
            raise Exception

        else:
            LOG.info("Completed: enable boot config for out_address {}".format(
                     self.out_address))

    def _set_boot_device(self, ipmi_boot_device):
        now = time.time()
        last_boot_change = self.AMT_ACTION_CACHE.get('ipmi_boot_device', 0)
        diff_boot_change = now - last_boot_change

        LOG.info('\nAMT_ACTION_CACHE\n'
                '    {}\n'.format(
                    pprint.pformat(self.AMT_ACTION_CACHE,
                        )))

        LOG.info('Last ipmi_boot_device change was {}. '
                'Compared to {} the difference is {}seconds.'.format(
                    last_boot_change,
                    now,
                    diff_boot_change))

        if diff_boot_change > self.AMT_ACTION_INTERVAL:

            LOG.debug('target boot device: {}'.format(
                ipmi_boot_device))
            LOG.debug('boot device before setting: {}'.format(
                self.ipmi_boot_device))
        
        
            if (type(ipmi_boot_device) in  [ str ]
                    and ipmi_boot_device in ipmicommand.boot_devices):
                self._set_boot_device_order(ipmi_boot_device = ipmi_boot_device)
                self._enable_boot_config()
                # we cannot set boot device persistent, only for next boot
                #self.ipmi_boot_device = ipmi_boot_device
                self.AMT_ACTION_CACHE['ipmi_boot_device'] = now
            else:
                raise Exception
        
            LOG.debug('boot device after setting: {},'
                    'boot device for next boot {}'.format(
                        self.ipmi_boot_device,
                        ipmi_boot_device))

        else:
            LOG.info('ipmi_boot_device already set to "{}" seconds ago for '
                    'out_address {}'.format(
                     diff_boot_change, 
                     self.out_address))

    def _set_boot_device_and_dont_wait(self, ipmi_boot_device):
        try:
            self._set_boot_device(ipmi_boot_device = ipmi_boot_device)
        except Exception:
            LOG.warning("out BMC set ipmi_boot_device {} for out_address {} "
                        "failed.".format(
                        ipmi_boot_device, self.out_address))
            raise Exception

        LOG.info("Completed: boot device change request "
                 "to out_address {} for ipmi_boot_device {}".format(
                 self.out_address, ipmi_boot_device))

    def set_boot_device(self, bootdevice):
        self._set_boot_device_and_dont_wait(ipmi_boot_device=bootdevice)

    def cold_reset(self):
        # reset BMC not target system
        print('shutting down in response to BMC cold reset request')
        sys.exit(0)

    def _get_power_state(self):

        awake_bmc(self.out_address)

        namespace = CIM_AssociatedPowerManagementService

        try:
            doc = self.client.wsman_get(namespace)
        except Exception as e:
            LOG.exception("Failed to get power state for out_address {} "
                          "with error: {}.".format(
                          self.out_address, e))
            return POWER_ERROR

        item = "PowerState"
        try:
            power_state = xml_find(doc, namespace, item).text
        except AttributeError as e:
            LOG.exception("Failed to get power state for out_address {}"
                    .format(self.out_address))
            return POWER_ERROR

        return power_state

    def _generate_power_action_input(self, action):
        method_input = "RequestPowerStateChange_INPUT"
        namespace = CIM_PowerManagementService
    
        doc = pywsman.XmlDoc(method_input)
        root = doc.root()
        root.set_ns(namespace)
        root.add(namespace, 'PowerState', action)
    
        child = root.add(namespace, 'ManagedElement', None)
        child.add(_ADDRESS, 'Address', _ANONYMOUS)
    
        grand_child = child.add(_ADDRESS, 'ReferenceParameters', None)
        grand_child.add(_WSMAN, 'ResourceURI', CIM_ComputerSystem)
    
        g_grand_child = grand_child.add(_WSMAN, 'SelectorSet', None)
        g_g_grand_child = g_grand_child.add(_WSMAN, 'Selector', 'ManagedSystem')
        g_g_grand_child.attr_add(_WSMAN, 'Name', 'Name')
        return doc

    def _set_power_state(self, switch_state):

        now = time.time()
        last_power_switch = self.AMT_ACTION_CACHE.get(switch_state, 0)
        diff_power_switch = now - last_power_switch
  
        LOG.info('\nAMT_ACTION_CACHE\n'
                '    {}\n'.format(
                    pprint.pformat(self.AMT_ACTION_CACHE,
                        )))

        LOG.info('Last Power switch of state "{}" was {}. '
                'Compared to {} the difference is {}seconds.'.format(
                    switch_state, 
                    last_power_switch, 
                    now, 
                    diff_power_switch))

        if diff_power_switch > self.AMT_ACTION_INTERVAL:

            awake_bmc(self.out_address)

            doc = self._generate_power_action_input(switch_state)

            LOG.debug('\ndoc(type {}):\n'
                    '    {}'.format(
                        type(doc), doc))
             
            method = 'RequestPowerStateChange'

            options = pywsman.ClientOptions()
            options.add_selector(
                    'Name', 
                    'Intel(r) AMT Power Management Service')
            
            try:
                client_doc = self.client.wsman_invoke(
                        options, 
                        CIM_PowerManagementService,
                        method, 
                        doc)
            except Exception as e:
                LOG.exception("Failed to set power state {} for out_address {} "
                              "with error: {}.".format(
                              switch_state, self.out_address, e))
                raise Exception
            else:
                self.AMT_ACTION_CACHE[switch_state] = now
                LOG.info("Power state set to {} for out_address {}".format(
                         switch_state, self.out_address))

        else:
            LOG.info('Power state already set to "{}" {}seconds ago for '
                    'out_address {}'.format(
                     switch_state, 
                     diff_power_switch, 
                     self.out_address))

    def _set_power_state_and_wait(self, switch_state, target_state=None):
        if target_state is None:
            target_state=switch_state

        if target_state not in (POWER_ON, POWER_OFF):
            raise Exception

        status = {'power': None, 'iter': 0, 'switch': False}
        LOG.info('after init: '
                'status["power"]: {}, '
                'status["iter"]: {}, '
                'status["switch"]: {}'.format(
                    status['power'], 
                    status['iter'], 
                    status['switch']))
        while True:
            status['power'] = self._get_power_state()
            LOG.info('in while: '
                'status["power"]: {}, '
                'status["iter"]: {}, '
                'status["switch"]: {}'.format(
                    status['power'], 
                    status['iter'], 
                    status['switch']))
            
            if status['power'] == target_state and \
                    switch_state == target_state:
                break
            elif status['power'] == target_state and \
                    switch_state != target_state and \
                    status['switch']:
                break

            if status['iter'] >= self.AMT_MAX_ATTEMPTS:
                status['power'] = POWER_ERROR
                LOG.warning('out BMC failed to set power state {} '
                        'to reach {} after '
                        '{} retries on out_address {}.'.format(
                            target_state, 
                            status['iter'],
                            self.out_address))
                break

            if not status['switch']:
                try:
                    self._set_power_state(switch_state)
                except Exception:
                    # Log failures but keep trying
                    LOG.warning('out BMC set power state {} to reach {} '
                                'for out_address {} '
                                '- Attempt {} times of {} '
                                'failed.'.format(
                                switch_state, 
                                target_state, 
                                self.out_address,
                                status['iter'] + 1,
                                self.AMT_MAX_ATTEMPTS))
                    raise Exception
                else:
                    status['switch']=True

            status['iter'] += 1
            time.sleep(self.AMT_ACTION_WAIT)

        if status['power'] != target_state:
            raise Exception

        LOG.info('Power state successfully reached {} '
                'for out_address {}'.format(
                 status['power'], 
                 self.out_address))
    
        return status['power']



    def _set_power_state_and_dont_wait(self, switch_state):
        status = {'power': None }

        status['power'] = self._get_power_state()

        LOG.info('after self._get_power_state(): '
                'status["power"]: {}'.format(status['power'] ))

        if status['power'] == switch_state:
            return switch_state

        try:
            self._set_power_state(switch_state)
        except Exception:
            LOG.warning("out BMC set power state {} for out_address {} "
                        "failed.".format(
                        switch_state, self.out_address))
            raise Exception


        LOG.info("Sent successfully power state change request "
                 "to out_address {} for power state {}".format(
                 self.out_address, switch_state))

        return status['power']

    def get_power_state(self):
        if self._get_power_state() in [ POWER_ON ]:
            return "on"
        else:
            return "off"

    def power_off(self):
        # this should be power down without waiting for clean shutdown
        #self._set_power_state_and_wait(switch_state=POWER_OFF)
        self._set_power_state_and_dont_wait(switch_state=POWER_OFF)

    def pulse_diag(self):
        #self._set_power_state_and_wait(switch_state=POWER_DIAG)
        self._set_power_state_and_dont_wait(switch_state=POWER_DIAG)

    def power_on(self):
        #self._set_power_state_and_wait(switch_state=POWER_ON)
        self._set_power_state_and_dont_wait(switch_state=POWER_ON)

    def power_reset(self):
        #self._set_power_state_and_wait(switch_state=POWER_CYCLE, \
        #        target_state=POWER_ON)
        self._set_power_state_and_dont_wait(switch_state=POWER_CYCLE)

    def power_shutdown(self):
        # should attempt a clean shutdown
        #self._set_power_state_and_wait(switch_state=POWER_OFF_GRACEFUL, \
        #        target_state=POWER_OFF)
        self._set_power_state_and_dont_wait(switch_state=POWER_OFF_GRACEFUL)

    def is_active(self):
        return self.get_power_state() == 'on'

    def iohandler(self, data):
        print(data)
        if self.sol:
            self.sol.send_data(data)


## MAIN
def main():
    global loglevel

    parser = argparse.ArgumentParser(
        prog='bmcconvert',
        description='implement input BMC interface and '
                'convert requests to target BMC interface/protcol',
    )
    requiredParser = parser.add_argument_group('required arguments')

    parser.add_argument('--in-address',
                        dest='in_address',
                        default='::',
                        help=('IN protocolbind address (IPv4 and IPv6 '
                              'are supported); defaults to ::'))
    parser.add_argument('--in-port',
                        dest='in_port',
                        type=int,
                        default=623,
                        help='IN protocollisten port; defaults to 623')
    parser.add_argument('--in-username',
                        dest='in_username',
                        default='admin',
                        help='IN protocolusername; defaults to "admin"')
    parser.add_argument('--in-password',
                        dest='in_password',
                        default='password',
                        help='IN protocolpassword; defaults to "password"')
    requiredParser.add_argument('--out-address',
                        required=True,
                        dest='out_address',
                        help=('The address of the out BMC'))
    parser.add_argument('--out-protocol',
                        dest='out_protocol',
                        default='http',
                        help=('The protocol for the out BMC '
                              'connection; defaults to http'))
    parser.add_argument('--out-username',
                        dest='out_username',
                        default='admin',
                        help=('The username for the out BMC '
                              'connection; defaults to "admin"'))
    requiredParser.add_argument('--out-password',
                        required=True,
                        dest='out_password',
                        help=('The password for the out BMC '
                              'connection'))
    parser.add_argument('--verbose',
                        dest='verbose',
                        action="store_true",
                        help=('enable verbose output'))
    parser.add_argument('--debug',
                        dest='debug',
                        action="store_true",
                        help=('enable debug output'))

    args = parser.parse_args()

    if args.verbose:
        loglevel=logging.INFO
    elif args.debug:
        loglevel=logging.NOTSET

    LOG.setLevel(loglevel)
    LOG.info('Loglevel: {}'.format(
        logging.getLevelName(LOG.getEffectiveLevel())))

    ipmiToAmt = IpmiToAmt(
            in_address = args.in_address,
            in_port    = args.in_port,
            authdata     = {args.in_username: args.in_password},
            out_address  = args.out_address,
            out_protocol = args.out_protocol,
            out_username = args.out_username,
            out_password = args.out_password)

    ipmiToAmt.listen()

if __name__ == '__main__':
    sys.exit(main())
